export interface RubikCipherConfig {
  cubeSize?: number;
  groupSize?: number;
  fillRest?: boolean;
  fillWithRandom?: boolean;
}

export interface RubikCipherConfigRequired {
  cubeSize: number;
  groupSize: number;
  fillRest: boolean;
  fillWithRandom: boolean;
}

export interface RubikKeyParts {
  config: RubikCipherConfigRequired;
  combinationText: string;
}

export interface ScrambleObject {
  depth: number;
  wide: boolean;
  target: string;
  rotation: number;
}

export interface FaceObject {
  originalIndex: number;
  value: number;
  index: number;
  assigned?: string;
}

export interface CubeObject {
  [index: string]: FaceObject[];
}
