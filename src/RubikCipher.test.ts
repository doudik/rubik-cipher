import { expect } from 'chai';
import RubikCipher from './RubikCipher';
import {
  chunkString, minimalSizeByText, getInverseCombination,
  getDataFromKey, encodeScramble, decodeScramble, isValidKey,
} from './helpers/rubikHelpers';

describe('rubikHelpers test', () => {
  it('chunkString', () => {
    expect(chunkString('aaaa', 2)).to.eql(['aa', 'aa']);
  });

  it('minimalSizeByText', () => {
    expect(minimalSizeByText('testtesttesttesttesttesttesttesttesttesttesttesttesttesttesttest')).to.equal(4);
  });

  it('encodeScramble', () => {
    expect(encodeScramble({
      depth: 1, wide: false, target: 'B', rotation: 1,
    })).to.equal('B011');

    expect(encodeScramble({
      depth: 2, wide: false, target: 'B', rotation: 1,
    })).to.equal('B021');

    expect(encodeScramble({
      depth: 3, wide: true, target: 'B', rotation: 1,
    })).to.equal('B131');

    expect(encodeScramble({
      depth: 1, wide: true, target: 'B', rotation: 1,
    })).to.equal('B111');

    expect(() => {
      encodeScramble({
        depth: 1, wide: true, target: 'B', rotation: 4,
      });
    }).to.throw();
  });

  it('decodeScramble', () => {
    expect(decodeScramble('B011')).to.eql({ depth: 1, wide: false, target: 'B', rotation: 1 });

    expect(decodeScramble('B021')).to.eql({ depth: 2, wide: false, target: 'B', rotation: 1 });

    expect(decodeScramble('B131')).to.eql({ depth: 3, wide: true, target: 'B', rotation: 1 });

    expect(decodeScramble('B111')).to.eql({ depth: 1, wide: true, target: 'B', rotation: 1 });

    expect(() => {
      decodeScramble('B11');
    }).to.throw();
  });

  it('isValidKey', () => {
    expect(isValidKey('LoremIpsum', '+5+1+1+L011 L123')).to.be.equal(false);
    expect(isValidKey('LoremIpsum', '1+L011')).to.be.equal(false);
    expect(isValidKey('LoremIpsum', '1+1+1+0+L112 U112 B013 D111')).to.be.equal(false);
    expect(isValidKey('LoremIpsum', '2+0+1+0+L112 U112 B013 D111')).to.be.equal(false);
    expect(isValidKey('LoremIpsumLoremIpsumLoremIpsum', '2+1+1+0+L112 U112 B013 D111')).to.be.equal(false);
  });

  it('getDataFromKey', () => {
    expect(() => {
      getDataFromKey('LoremIpsum', '1+L011');
    }).to.throw();
  });
});

describe('Cube test', () => {
  it('Create cube without config', () => {
    expect(() => {
      const result = new RubikCipher('test');
    }).to.not.throw();
  });

  it('Create cube with empty text', () => {
    expect(() => {
      const result = new RubikCipher('');
    }).to.throw();
  });

  it('Create cube with config object', () => {
    expect(() => {
      const result = new RubikCipher('test', { cubeSize: 2, groupSize: 1, fillRest: true, fillWithRandom: false });
    }).to.not.throw();

    expect(() => {
      const result = new RubikCipher('test', { });
    }).to.not.throw();
  });

  it('Create cube with config that has invalid size', () => {
    expect(() => {
      const result = new RubikCipher('test', { cubeSize: 1 });
    }).to.throw();
  });

  it('Create cube with config, that has text longer than specified size', () => {
    expect(() => {
      const result = new RubikCipher('testtesttesttesttesttesttesttest', { cubeSize: 2 });
    }).to.throw();
  });

  it('Create cube with config that has invalid group size', () => {
    expect(() => {
      const result = new RubikCipher('test', { groupSize: 0 });
    }).to.throw('Group size must be larger than 0');
  });

  it('Create cube with key config', () => {
    expect(() => {
      const result = new RubikCipher('test', '2+1+1+0+L112 U112 B013 D111');
    }).to.not.throw();
  });

  it('Create cube with key config, that has invalid size', () => {
    expect(() => {
      const result = new RubikCipher('test', '1+1+1+0+L112 U112 B013 D111');
    }).to.throw();
  });

  it('Create cube with key config, that has invalid group size', () => {
    expect(() => {
      const result = new RubikCipher('test', '2+0+1+0+L112 U112 B013 D111');
    }).to.throw();
  });

  it('Create cube with key config, that has too long text', () => {
    expect(() => {
      const result = new RubikCipher('testtesttesttesttesttesttesttesttesttesttesttesttesttesttesttest', '2+1+1+0+F R- L');
    }).to.throw();
  });

  it('Create cube with key config, that has invalid format', () => {
    expect(() => {
      const result = new RubikCipher('test', '+2+1+1+0+L112 U112 B013 D111');
    }).to.throw();

    expect(() => {
      const result = new RubikCipher('test', '2+1+aabb');
    }).to.throw();
  });

  it('Scramble cube with no parameters, 2^3 times', () => {
    const result = new RubikCipher('test');
    result.scramble();
    expect(result.Combination.split(' ').length).to.equal(2 ** 3);
  });

  it('Scramble cube 5 times', () => {
    const result = new RubikCipher('test');
    result.scramble(5);
    expect(result.Combination.split(' ').length).to.equal(5);
  });

  it('Scramle cube 5 times and add 5 scrambles more', () => {
    const result = new RubikCipher('test');
    result.scramble(5);
    result.addScramble(5);
    expect(result.Combination.split(' ').length).to.equal(10);
  });

  it('Scramble cube 5 times and add 2^3 scrambles', () => {
    const result = new RubikCipher('test');
    result.scramble(5);
    result.addScramble();
    expect(result.Combination.split(' ').length).to.equal(5 + (2 ** 3));
  });

  it('Turn cube by string combination', () => {
    const result = new RubikCipher('test');
    result.turn('L112 U112 B013 D111');
    expect(result.Combination).to.equal('L112 U112 B013 D111');
  });

  it('Turn cube by object array combination', () => {
    const result = new RubikCipher('test');
    result.turn([{ depth: 1, wide: false, target: 'U', rotation: 1 }]);
    expect(result.Combination).to.equal('U011');
  });

  it('Turn cube with specific inverse, size 4', () => {
    const result = new RubikCipher('Testing is cool.', { cubeSize: 4 });
    const baseIndexes = result.Indexes;
    result.turn('B111 L012 B113 D111 B013');
    expect(result.Combination).to.equal('B111 L012 B113 D111 B013');
    result.turn('B011 D113 B111 L013 L013 B113');
    expect(result.Indexes).to.be.eql(baseIndexes);
  });

  it('Turn cube with inverse function, size 5, random shuffle', () => {
    const result = new RubikCipher('Testing is cool.', { cubeSize: 5 });
    const baseIndexes = result.Indexes;
    expect(result.InverseCombination).to.be.eql([]);
    result.scramble();
    expect(result.Indexes).to.not.be.eql(baseIndexes);
    result.turn(result.InverseCombination);
    expect(result.Indexes).to.be.eql(baseIndexes);
  });

  it('Text from default cube (x filled)', () => {
    const result = new RubikCipher('test');
    expect(result.Text).to.equal('testxxxxxxxxxxxxxxxxxxxx');
  });

  it('Text from cube with random fill', () => {
    const word = 'test';
    const result = new RubikCipher(word, { fillWithRandom: true });
    const text = result.Text;
    expect(text.substr(0, word.length)).to.equal(word);
  });

  it('Text from cube with no fill', () => {
    const result = new RubikCipher('test', { fillRest: false });
    const text = result.Text;
    expect(text).to.equal('test');
  });

  // Todo: Add changeSettings to RubikCipher()
  it('Text from cube with groups by 2', () => {
    let cube = new RubikCipher('testandtest', { groupSize: 2 });
    cube.scramble(5);
    expect(cube.Text.length).to.equal((6 * (2 ** 2) * 2));

    cube = new RubikCipher('testtest', { groupSize: 2 });
    cube.scramble(5);
    expect(cube.Text.length).to.equal((6 * (2 ** 2) * 2));

    cube = new RubikCipher('testandtest', { groupSize: 2, fillWithRandom: true });
    cube.scramble(5);
    expect(cube.Text.length).to.equal((6 * (2 ** 2) * 2));
  });

  it('Text from cube with groups by 2, but with no fill', () => {
    let cube = new RubikCipher('testtest', { groupSize: 2, fillRest: false });
    cube.scramble(5);
    expect(cube.Text.length).to.equal(8);

    cube = new RubikCipher('testteste', { groupSize: 2, fillRest: false });
    cube.scramble(5);
    expect(cube.Text.length).to.equal(9);
  });

  it('Text from cube, that has been scrambled and unscrabled', () => {
    const result = new RubikCipher('test');
    expect(result.Text).to.equal('testxxxxxxxxxxxxxxxxxxxx');
    result.turn('B111 L012 B113 D111 B013');
    expect(result.Text).to.equal('xsxtxtxxxxxexxxxxxxxxxxx');
    result.turn('B011 D113 B111 L013 L013 B113');
    expect(result.Text).to.equal('testxxxxxxxxxxxxxxxxxxxx');
  });

  it('Text from cube with large cube config', () => {
    expect(() => {
      const cube = new RubikCipher('test object test', '5+1+1+1+B012 L022 D113 L013 F112');
    }).to.not.throw();
  });

  // Todo: Add changeSettings to RubikCipher()
  it('Key from cube with specified config object', () => {
    let cube = new RubikCipher('test', {
      cubeSize: 2,
      groupSize: 1,
      fillRest: true,
      fillWithRandom: true,
    });
    cube.turn('L112 U112 B013 D111');
    expect(cube.Key).to.be.equal('2+1+1+1+L112 U112 B013 D111');

    cube = new RubikCipher('test', {
      cubeSize: 2,
      groupSize: 1,
      fillRest: false,
      fillWithRandom: false,
    });
    cube.turn('L112 U112 B013 D111');
    expect(cube.Key).to.be.equal('2+1+0+0+L112 U112 B013 D111');
  });

  it('Decode dynamic cube from key, from size 2 to 10', () => {
    const testCount = 10;
    for (let i = 2; i < testCount; i++) {
      const cCube = new RubikCipher('HelloWorld!', { cubeSize: i });
      const originalText = cCube.Text;
      cCube.scramble();
      const ciphreKey = cCube.Key;
      const ciphreText = cCube.Text;

      const result = new RubikCipher(ciphreText, ciphreKey);
      expect(result.Text).to.be.equal(originalText);
    }
  });

  it('Decode dynamic cube from key but standart constructor, from size 2 to 10', () => {
    const testCount = 10;
    for (let i = 2; i < testCount; i++) {
      const cCube = new RubikCipher('HelloWorld!', { cubeSize: i });
      const originalText = cCube.Text;
      cCube.scramble();
      const cCombination = cCube.Combination;
      const cKey = cCube.Key;
      const ciphreText = cCube.Text;

      const config = getDataFromKey(ciphreText, cKey);
      expect(cKey.split('+')[4]).to.be.equal(cCombination);
      const testCiphreCube = new RubikCipher(ciphreText, config);
      expect(testCiphreCube.Text).to.be.equal(ciphreText);
      const inverse = getInverseCombination(cKey.split('+')[4].split(' ').map(decodeScramble));
      expect(inverse.map(encodeScramble).join(' ')).to.be.equal(cCube.InverseCombination.map(encodeScramble).join(' '));
      expect(inverse).to.be.eql(cCube.InverseCombination);
      testCiphreCube.turn(inverse);
      expect(testCiphreCube.Text).to.be.equal(originalText);
    }
  });
});