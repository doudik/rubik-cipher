# Rubik cipher

[![pipeline status](https://gitlab.com/honzad/rubik-cipher/badges/master/pipeline.svg)](https://gitlab.com/honzad/rubik-cipher/commits/master)
[![coverage report](https://gitlab.com/honzad/rubik-cipher/badges/master/coverage.svg?job=test)](https://gitlab.com/honzad/rubik-cipher/commits/master)
[![npm version](https://badge.fury.io/js/rubik-cipher.svg)](https://badge.fury.io/js/rubik-cipher)

Basic implementation of rubik cipher.

## Installation

```sh
npm install rubik-cipher --save
```

## Usage
##### RubikCipherConfig
Configuration object that is passed when creating new RubikCipher
| field | type | default | required | explanation |
|:--------------:|:-------:|:-------:|:--------:|:-----------------------------------------------------------------:|
| cubeSize | number | 2 | no | How large is the cube (n*n)) |
| groupSize | number | 1 | no | By how many chars group the divided string |
| fillRest | boolean | true | no | If the rest of cube spaces will be filled or left empty |
| fillWithRandom | boolean | false | no | If the spaces will be filled by char 'x' or with random character |

### **JavaScript**
##### Creation
---
```javascript
const RubikCipher = require('rubik-cipher');

// Creates cube with default parameters
const defaultCube = new RubikCipher('hello world!');

// Creates cube with RubikCipherConfig
const configCube = new RubikCipher('hello world!', {
  cubeSize: 2,
  groupSize: 1,
  fillRest: true,
  fillWithRandom: true,
});

// Creates and decodes cube with selected key config
// keyCube.Text should be 'testxxxxxxxxxxxxxxxxxxxx'
const keyCube = new RubikCipher('xsxtxtxxxxxexxxxxxxxxxxx', '2+1+1+0+B111 L012 B113 D111 B013');
```
##### Values
---
```javascript
// By using default parameters the fillRest is true, so the rest of the string after typed text is char 'x' 
// example: 'hello world!xxxxxxxxxxxx'
const currentText = cube.Text; 

// Generated combination that follows pattern: 'B111 L012 B113 D111 B013'
// B113 -> move+isWide+depth+rotation -> move = B, isWide = 1 (true), depth = 1, rotation = 3 (3 equals to -1)
const currentRandomCombination = cube.Combination;

// Gets the current key that is in format: 'cubeSize+groupSize+fillRest+fillWithRandom+Combination'
// example: '2+1+1+0+B111 L012 B113 D111 B013'
// where cubeSize is 2, groupSize is 1, fillRest is true, fillWithRandom is false and Combination is 'B111 L012 B113 D111 B013'
const currentKey = cube.Key;
```
##### Modifications
---
```javascript
// Scrambles the cube cubeSize^3 times
cube.scramble();

// Scrambles the cube 20 times.
cube.scramble(20);

// Adds cubeSize^3 more generated moves to the combination.
cube.addScramble();

// Adds 20 more generated moves to the combination.
cube.addScramble(20);

// Manual string sequence turn
cube.turn('L112 U112 B013 D111');

// Manual object array sequence turn
cube.turn([{ depth: 1, wide: false, target: 'U', rotation: 1 }]);

// Clears the cube of scrambles
cube.reset();
```
##### Examples
---
```javascript
const RubikCipher = require('rubik-cipher');
const cube = new RubikCipher('HelloWorld!', {
  groupSize: 1,
  fillRest: true,
  fillWithRandom: false,
});
cube.scramble();

const ciphreKey = cube.Key;
const ciphreText = cube.Text;

const result = new RubikCipher(ciphreText, ciphreKey);
const decodedText = result.Text;
```

### TypeScript

```typescript
import RubikCube from 'rubik-cipher';
const cube = new RubikCipher('HelloWorld!', {
  groupSize: 1,
  fillRest: true,
  fillWithRandom: false,
});
cube.scramble();

const ciphreKey = cube.Key;
const ciphreText = cube.Text;

const result = new RubikCipher(ciphreText, ciphreKey);
const decodedText = result.Text;
```

## Test

```sh
npm run test
```
